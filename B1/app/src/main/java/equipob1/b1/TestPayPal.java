package equipob1.b1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;

public class TestPayPal extends AppCompatActivity {
        TextView m_response;

        PayPalConfiguration m_configuration;
        String m_paypalClientId = "AYTn-aZLAx3shVWGiZ3OBnQR9VbF4qGJWCdJubFTJr_Or13GHIdVB5YC0XO4IyqgEbFerqaRKmikEFls";
        Intent m_service;
        int m_paypalRequestCode = 999;

        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_test_pay_pal);


            m_response = (TextView) findViewById(R.id.response);

            m_configuration = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(m_paypalClientId); //sandbox for test

            m_service = new Intent(this, PayPalService.class);

            m_service.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuration); //configuration above

            startService(m_service); //paypal service, listening to calls to paypal app
        }

    void pay(View view) {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(10), "USD", "Test payment with Paypal", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, m_paypalRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == m_paypalRequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                //tenemos que confirmar que el pago funcionó para evitar fraudes
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirmation != null) {
                    String state = confirmation.getProofOfPayment().getState();

                    if (state.equals("approved")) //si el pago se realizo con exito
                        m_response.setText("\n\n\nyour donation was successful");
                    else
                        m_response.setText("\n\n\ndonation error");

                } else
                    m_response.setText("\n\n\nConfirmation is null");
            }
        }
    }
}
