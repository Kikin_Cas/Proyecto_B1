package equipob1.b1;

import java.util.Date;

/**
 * Created by Kikin on 21/10/2017.
 */

public class Proyecto {
    private String nombre;
    private String descripcion;
    private Date fecha_limite;
    private float monto;

    public Proyecto(String nombre, String descripcion, Date fecha_limite, float monto) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fecha_limite = fecha_limite;
        this.monto = monto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha_limite() {
        return fecha_limite;
    }

    public void setFecha_limite(Date fecha_limite) {
        this.fecha_limite = fecha_limite;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }
}
